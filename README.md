1.  Create a single HTML document named “index.html” with all appropriate sections.

2.  Include the title (“Assignment #X – LastName, FirstName”) at the top of the page using a "h1" tag.  This must be centered.

3.  Create an external stylesheet called “style.css”.  This file will contain all of your CSS for this assignment. There is to be no inline or document level CSS for this assignment.

4.  Inside the document "head", include the appropriate "title" tag and "meta" tags.

5.  Inside the document "head", include the appropriate tag for including the external stylesheet that you created in step 2.

6.  Create the following sections and their appropriate contents.  Each section should be defined by the appropriate tags.  Each section should be labeled with an appropriate heading.  The heading must be inside the appropriate tag and be styled to be larger than the section text and centered. The sections are as follows:
    Section 1. Create a table that displays the weekly scores from a sport of your choice.   The table must contain information for a minimum of 5 games and should include the name of each team involved in the game, the record of each team, and the final score of the game.  The winning score must be larger, bold-faced, and in a different font than the losing score.  The table must also have appropriate borders/styling.

    Section 2. Create a section that contains two images of your choice along with enough text to flow around each of the images. The images should be no larger than 200px wide. One image should float to the left and the other image should float to the right.  The text should be justified.  Sufficient text may be generated using http://www.lipsum.com/.
    
    Section 3. Create a new section that consists of 3 columns.  The columns may be created using a "table" or "div" tags that are styled using CSS. Fill each column with the content created in Section 2 of your previous lab (in other words, you should replicate the content created in Section 2 of Lab 3 a total of 3 times), but make sure that each unordered list has a different bullet style and each ordered list has a different numbering style.

7.  Create a menu at the top of the page, directly below the title you created in Step 1 using an unordered list that is styled using CSS. In order to accomplish this task take the following steps:
A)  Search for the term “css3 menu list” using your favorite search engine;
B)  Once you find a menu that you like, integrate the provided HTML and CSS into your project.  The integration must be clean and all universal selectors must be removed.